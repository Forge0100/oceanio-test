import styled from "styled-components";

const Line = styled.div`
    background-color: ${props => props.active ? '#FFFFFF' : '#6E7986'};
    height: 4px;
    margin: auto 2px;
    flex-grow: 1;
    cursor: pointer;
`;

export default Line;