import styled from "styled-components";

const Navigator = styled.div`
    display: flex;
    width: 600px;
    margin: 36px auto;
`;

export default Navigator;