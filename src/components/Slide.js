import styled from "styled-components";

const Slide = styled.div`
    display: flex;
    flex-direction: column;
`;

export default Slide;