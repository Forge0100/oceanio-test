import React from 'react';
import styled from 'styled-components';
import Slide from '../Slide';
import Quotes from '../Quotes';
import pancake from '../../assets/pancake.svg'

const Quote = styled.p`
    font-size: 40px;
    font-weight: bold;
    letter-spacing: 0.8px;
    margin: 20px auto;
    text-align: center;
    line-height: 50px;
`;

const PinkText = styled.span`
    color: #FF88AD;
`;

const MiniProfile = styled.div`
    display: flex;
    flex-direction: column;
`;

const Avatar = styled.img`
    margin: 5px auto;
`;

const ProfileName = styled.p`
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    letter-spacing: 0px;
    line-height: 22px;
    margin: 5px auto;
`;

const Slide1 = () => (
    <Slide>
        <Quotes stroke="#ff88ad" />
        <Quote>An experience that makes us feel like we’re covered in maple syrup – <PinkText>It’s a good thing</PinkText></Quote>
        <MiniProfile>
            <Avatar src={pancake} />
            <ProfileName>
                The whole team @<br/>
                The Pancake Collective&trade;
            </ProfileName>
        </MiniProfile>
    </Slide>
);

export default Slide1;