import React from 'react';
import styled from 'styled-components';
import Slide from '../Slide';
import Quotes from '../Quotes';
import face from '../../assets/face.svg'

const Quote = styled.p`
    font-size: 40px;
    font-weight: bold;
    letter-spacing: 0.8px;
    margin: 20px auto;
    text-align: center;
    line-height: 50px;
`;

const Highlighting = styled.span`
    color: #88FFED;
`;

const MiniProfile = styled.div`
    display: flex;
    flex-direction: column;
`;

const Avatar = styled.img`
    margin: 5px auto;
`;

const Name = styled.p`
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    letter-spacing: 0px;
    margin: 5px auto;
`;

const Status = styled.p`
    font-size: 14px;
    text-align: center;
    margin: 0 auto;
`;

const Slide2 = () => (
    <Slide>
        <Quotes stroke="#88FFED" />
        <Quote>This piece of software is <Highlighting>the shit</Highlighting>, I’ve never tried anything like it.</Quote>
        <MiniProfile>
            <Avatar src={face} />
            <Name>Mr. Face</Name>
            <Status>Karma Guru @ Face Co&trade;</Status>
        </MiniProfile>
    </Slide>
);

export default Slide2;