import styled from "styled-components";

const Slider = styled.div`
    max-width: 865px;
    height: 335px;
`;

export default Slider;