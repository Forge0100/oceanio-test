import React, { useState } from 'react';
import Main from './components/Main';
import Slider from './components/Slider';
import Slide1 from './components/Slides/Slide1';
import Slide2 from './components/Slides/Slide2';
import Navigator from './components/Navigator';
import Line from './components/Line';
import './App.css';

const slides = [Slide1, Slide2];

function App() {
  const [slide, setSlide] = useState(0);
  const Slide = slides[slide];
  const lines = slides.map((_, index) => (
    <Line 
      key={index}
      active={index === slide}
      onClick={() => setSlide(index)} 
     />
  ));

  return (
    <Main>
      <Slider>
        <Slide />
      </Slider>
      <Navigator>
        {lines}
      </Navigator>
    </Main>
  );
}

export default App;
